package co.starmandevs.ed.project.model;

import co.starmandevs.ed.list.List;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class Evaluation {

    @NotNull
    private Integer evaluator;

    @Max(20)
    private Integer aspect1;

    @Max(10)
    private Integer aspect2;

    @Max(15)
    private Integer aspect3;

    @Max(20)
    private Integer aspect4;

    @Max(15)
    private Integer aspect5;

    @Max(15)
    private Integer aspect6;

    @Max(5)
    private Integer aspect7;

    private List<String> ethicalConsiderations = new List<>();

    private List<String> budgetConsiderations = new List<>();
}
