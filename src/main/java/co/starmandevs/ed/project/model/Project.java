package co.starmandevs.ed.project.model;

import co.starmandevs.ed.stack.Stack;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class Project {

    @NotNull
    private Integer id;

    @NotNull
    private String name;

    private Stack<Evaluation> evaluationStack = new Stack<>(3);
}
