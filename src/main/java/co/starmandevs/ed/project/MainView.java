package co.starmandevs.ed.project;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.NativeButtonRenderer;
import org.springframework.util.StringUtils;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;

@Route
public class MainView extends Div {

    public MainView() {

        HorizontalLayout overGridButtonsLayout = new HorizontalLayout();
        HorizontalLayout gridLayout = new HorizontalLayout();
        add(overGridButtonsLayout, gridLayout);

        Button button1 = new Button("button1", VaadinIcon.PLUS.create());
        Button button2 = new Button("button2", VaadinIcon.PLUS.create());
        Button button3 = new Button("button3", VaadinIcon.PLUS.create());
        overGridButtonsLayout.add(button1, button2, button3);

        Grid<GridContentDemo> grid = new Grid();
        grid.addColumn(GridContentDemo::getCol1).setHeader("col1");
        grid.addColumn(GridContentDemo::getCol2).setHeader("col2");
        grid.addColumn(new NativeButtonRenderer<GridContentDemo>("button1", clickEvent -> {
            System.out.println("hola mundo! " + clickEvent.getCol1());
        })).setHeader("Action");
        grid.addColumn(new ComponentRenderer<>(
                () -> VaadinIcon.ARROW_LEFT.create()));
        grid.addColumn(new ComponentRenderer<>(
                () -> new Button("otro boton", VaadinIcon.PLUS.create())));

        grid.setItems(new GridContentDemo("1","1"), new GridContentDemo("2","2"), new GridContentDemo("3","3"));
        gridLayout.add(grid);

        
    }
}
