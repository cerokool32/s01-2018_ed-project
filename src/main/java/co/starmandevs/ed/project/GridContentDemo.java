package co.starmandevs.ed.project;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GridContentDemo {
    private String col1;
    private String col2;
}
